﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;
using SQLite;
using Android.Content;

namespace Bookkeeper
{
	[Activity (Label = "CreateNewEvent", MainLauncher = true, Icon = "@mipmap/icon")]
	/// <summary>
	/// Create new event Activity, takes information from the user and saves it to BookKeeperManager.
	/// </summary>
	public class CreateNewEvent : Activity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView (Resource.Layout.CreateNewEvent);

			// get information from BookkeeperManager to Arrays
			BookkeeperManager bookKeeper = BookkeeperManager.Instance;
			List <Account> incomeListAccount = bookKeeper.AccountListIncome;
			string[] incomeAccountArray = createAnArray (incomeListAccount);
			List <Account> expenseListArray = bookKeeper.AccountListOutcome;
			string[] expenseAccountArray = createAnArray (expenseListArray);
			List <Account> moneyAccountList = bookKeeper.MoneyAccountList;
			string[] moneyAccountArray = createAnArray (moneyAccountList);
			List<TaxRate> taxRateList = bookKeeper.TaxRates;
			string[] taxRateListArray = createAnArrayDouble (taxRateList);
			bool isIncome = true;

			// connect with layout
			RadioButton radioIncome = FindViewById<RadioButton> (Resource.Id.radio_button_income);
			RadioButton radioExpense = FindViewById<RadioButton> (Resource.Id.radio_button_outcome);

			Spinner typeSpinner = FindViewById<Spinner> (Resource.Id.spinner_type);
			Spinner moneySpinner = FindViewById<Spinner> (Resource.Id.spinner_to_from);
			Spinner taxSpinner = FindViewById<Spinner> (Resource.Id.spinner_sales);

			Button addButton = FindViewById<Button> (Resource.Id.add_button);

			EditText dateEditText = FindViewById<EditText> (Resource.Id.date_edit_text);
			EditText descriptionEditText = FindViewById<EditText> (Resource.Id.description_edit_text);
			EditText moneyEditText = FindViewById<EditText> (Resource.Id.total_edit_text);

			// Creates adapters for spinners
			var typeAdapterIncome = new ArrayAdapter<string>(
				this, Android.Resource.Layout.SimpleSpinnerItem, incomeAccountArray);
			typeAdapterIncome.SetDropDownViewResource
			(Android.Resource.Layout.SimpleSpinnerDropDownItem);

			var typeAdapterOutcome = new ArrayAdapter<string>(
				this, Android.Resource.Layout.SimpleSpinnerItem, expenseAccountArray);
			typeAdapterOutcome.SetDropDownViewResource
			(Android.Resource.Layout.SimpleSpinnerDropDownItem);

			var adapterMoneyAccount = new ArrayAdapter<string>(
				this, Android.Resource.Layout.SimpleSpinnerItem, moneyAccountArray);
			adapterMoneyAccount.SetDropDownViewResource
			(Android.Resource.Layout.SimpleSpinnerDropDownItem);

			var adapterTaxRate = new ArrayAdapter<string>(
				this, Android.Resource.Layout.SimpleSpinnerItem, taxRateListArray);
			adapterTaxRate.SetDropDownViewResource
			(Android.Resource.Layout.SimpleSpinnerDropDownItem);

			// populate spinners with Arrays
			taxSpinner.Adapter = adapterTaxRate;
			typeSpinner.Adapter = typeAdapterIncome;
			moneySpinner.Adapter = adapterMoneyAccount;

			// different arrays in typeSpinner depending on what radiobutton is clicked
			radioIncome.Click += delegate {
				typeSpinner.Adapter = typeAdapterIncome;
				isIncome = true;
			} ;

			radioExpense.Click += delegate {
				typeSpinner.Adapter = typeAdapterOutcome;
				isIncome = false;
			} ;

			// Saves to db Entry
			addButton.Click += delegate {

				if(dateEditText.Text == "" || descriptionEditText.Text == "" || moneyEditText.Text == "")
				{
					Toast.MakeText (this, GetString(Resource.String.fill_everything_alert), ToastLength.Long).Show();
				} else 
				{
				string type = typeSpinner.SelectedItem.ToString();
				int accountIndex = moneySpinner.SelectedItemPosition;
				int accountId = moneyAccountList[accountIndex].AccountNumber;
				int taxRateIndex = taxSpinner.SelectedItemPosition;
				int taxId = taxRateList[taxRateIndex]._id;
				string date = dateEditText.Text;
				string description = descriptionEditText.Text;
				int moneyValue = Int32.Parse(moneyEditText.Text);

				Entry entry =new Entry{AccountId = accountId, TaxRateId = taxId, Date = date, Type = type, Description = description, Money = moneyValue, IsIncome = isIncome};
				bookKeeper.AddEntry(entry);
				Intent intent = new Intent(this, typeof(EntryListActivity));
				StartActivity(intent);
				}
			};
		}	

		/// <summary>
		/// Takes a list of Accounts and turns it in to an string[] of accounts Name and AccountNumber
		/// </summary>
		/// <returns>string array with information about Accounts</returns>
		/// <param name="accountListForMethod">Account list for method.</param>
		public string[] createAnArray(List<Account> accountListForMethod)
		{
			int size = accountListForMethod.Count;
			string[] stringArray = new string[size];
			List <String> stringList = new List<String> ();

			foreach (Account a in accountListForMethod) 
			{
				stringList.Add (a.Name +" ("+ a.AccountNumber+") ");
			}

			for (int i = 0; i < size; i++) 
			{
				stringArray [i] = stringList[i];
			}

			return stringArray;
		}

		/// <summary>
		/// Creates an array of strings from a List of TaxRate
		/// </summary>
		/// <returns>The an array string.</returns>
		/// <param name="taxListForMethod">Tax list for method.</param>
		public string[] createAnArrayDouble(List<TaxRate> taxListForMethod)
		{
			int size = taxListForMethod.Count;
			string[] doubleArray = new string[size];
			List <String> doubleList = new List<String> ();

			foreach (TaxRate t in taxListForMethod) 
			{
				doubleList.Add (t.SalesRate.ToString());
			}

			for (int i = 0; i < size; i++) 
			{
				doubleArray [i] = doubleList [i] + "%";
			}

			return doubleArray;
		}
	}
}