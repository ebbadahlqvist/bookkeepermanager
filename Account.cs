﻿using System;
using SQLite;

namespace Bookkeeper
{
	/// <summary>
	/// Class that represents Account.
	/// </summary>
	public class Account
	{
		public string Name{ get; set;}
		public int AccountNumber{ get; set;}
		public bool IsIncome{ get; set;}

		/// <summary>
		/// Initializes a new instance of the <see cref="Bookkeeper.Account"/> class.
		/// </summary>
		public Account ()
		{
		}
	}
}

