﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper
{
	[Activity (Label = "EntryAdapter")]		
	/// <summary>
	/// Entry adapter.
	/// </summary>
	public class EntryAdapter : BaseAdapter
	{
		private Context aContext;
		private List <Entry> aEntries;

		/// <summary>
		/// Initializes a new instance of the <see cref="Bookkeeper.EntryAdapter"/> class.
		/// </summary>
		/// <param name="context">Context.</param>
		/// <param name="entries">Entries.</param>
		public EntryAdapter (Context context, List <Entry> entries)
		{
			aEntries = entries;
			aContext = context;
		}

		#region implemented abstract members of BaseAdapter
		public override Java.Lang.Object GetItem (int position)
		{
			return null;
		}
		public override long GetItemId (int position)
		{
			return position;
		}
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			
			View view = convertView; // re-use an existing view, if one is available
			if (view == null) {// otherwise create a new one
				view = LayoutInflater.From (aContext).Inflate (Resource.Layout.EntryListRow, null, false);
			}

			TextView entryTextView1 = view.FindViewById<TextView> (Resource.Id.textViewTable1);
			entryTextView1.Text = aEntries [position].Date;

			TextView entryTextView2 = view.FindViewById<TextView> (Resource.Id.textViewTable2);
			entryTextView2.Text = aEntries [position].Description;

			TextView entryTextView3 = view.FindViewById<TextView> (Resource.Id.textViewTable3);
			entryTextView3.Text = Convert.ToString(aEntries [position].Money);

			return view;
		}
			
		public override int Count {
			get {
				return aEntries.Count;
			}
		}
		#endregion	
	}
}