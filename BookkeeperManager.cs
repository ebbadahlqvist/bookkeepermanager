﻿
using System;
using System.Collections.Generic;
using SQLite;




namespace Bookkeeper
{
	/// <summary>
	/// Bookkeeper manager. Handles all the bookkeeping information. Singleton. 
	/// </summary>
	public class BookkeeperManager
	{
		private static BookkeeperManager instance = null;
		private List <Account> accountListExpense;
		private List <TaxRate> taxRateList;
		private List <Account> accountListIncome;
		private List <Account> moneyAccountList;
		private List <Entry> entries;
		string path = System.Environment.GetFolderPath
			(System.Environment.SpecialFolder.Personal);

		/// <summary>
		/// Counts toghether the totoal Tax of all events and returns a string with all the info you need
		/// for a taxReport
		/// </summary>
		/// <returns>The tax report. As a string.</returns>
		public string GetTaxReport()
		{
			double newTotal = 0;
			double taxRate = 0;
			string taxReport = "";
			double sum = 0;

			foreach (Entry e in entries) 
			{
				foreach (TaxRate t in taxRateList) 
				{
					if (e.TaxRateId == t._id) 
					{
						taxRate = t.SalesRate * 0.01;
						break;
					}
				}
				newTotal = e.Money - (e.Money / (1 + taxRate));

				if (!e.IsIncome) 
				{
					newTotal = newTotal * -1;
				}
				newTotal = Math.Round (newTotal,2);
				taxReport = taxReport + e.Date +"  -  "+ e.Description+",  "+newTotal +"kr"+ System.Environment.NewLine;
				sum = sum + newTotal;
			}

			taxReport = taxReport + "Totalt: " + sum + "kr."; 
			return taxReport;
		}


		/// <summary>
		/// Saves an entry in the SQL and in the list entries in bookkepper instance
		/// </summary>
		/// <param name="e">An entry</param>
		public void AddEntry(Entry e)
		{
			string path = System.Environment.GetFolderPath
				(System.Environment.SpecialFolder.Personal);
			SQLiteConnection db = new SQLiteConnection (path + @"\database.db");
			entries.Add (e);
			db.Insert (e);

		}
	
		/// <summary>
		/// Gets the instance of bookkeeper.
		/// </summary>
		/// <value>The instance.</value>
		public static BookkeeperManager Instance {
			get {
				if (instance == null) {
					instance = new BookkeeperManager ();
				}
				return instance;
			}
		}

		private BookkeeperManager ()
		{
			SQLiteConnection db = new SQLiteConnection (path + @"\database.db");
			dbCreator ();
			moneyAccountList = new List<Account> ();
			accountListIncome = new List<Account> ();
			accountListExpense = new List<Account> ();
			taxRateList = new List<TaxRate> ();
			populateLists ();
			entries = new List<Entry> ();
			var entryInDb = db.Table<Entry>();

			foreach (Entry e in entryInDb)
			{
				entries.Add (e);
			}
		}

		/// <summary>
		/// Gets the account list income.
		/// </summary>
		/// <value>The account list income.</value>
		public List<Account> AccountListIncome {
			get {
				return accountListIncome;
			}
		}

		/// <summary>
		/// Gets the money account list.
		/// </summary>
		/// <value>The money account list.</value>
		public List<Account> MoneyAccountList {
			get {
				return moneyAccountList;
			}
		}

		/// <summary>
		/// Gets the account list outcome.
		/// </summary>
		/// <value>The account list outcome.</value>
		public List<Account> AccountListOutcome {
			get {
				return accountListExpense;
			}
		}

		/// <summary>
		/// Gets the tax rates.
		/// </summary>
		/// <value>The tax rates.</value>
		public List<TaxRate> TaxRates {
			get {
				return taxRateList;
			}
		}

		/// <summary>
		/// Gets the entries.
		/// </summary>
		/// <value>The entries.</value>
		public List<Entry> Entries {
			get {
				return entries;
			}
		}
			
		private void populateLists(){
			SQLiteConnection db = new SQLiteConnection (path + @"\database.db");
			var accountsInDb = db.Table<Account> ();
		
			foreach (Account a in accountsInDb) 
			{
				if (a.AccountNumber < 2000) {
					accountListIncome.Add (a);

				} else if (a.AccountNumber < 3000) {
					accountListExpense.Add (a);

				} else 
				{
					moneyAccountList.Add (a);
				}
			}

			var taxRatesInDb = db.Table<TaxRate> ();

			foreach (TaxRate t in taxRatesInDb) 
			{
				taxRateList.Add (t);
			}	
		}
			
		private void dbCreator()
		{
			string path = System.Environment.GetFolderPath
				(System.Environment.SpecialFolder.Personal);
			SQLiteConnection db = new SQLiteConnection (path + @"\database.db");
			db.CreateTable<Entry>(); 
			db.CreateTable<Account> ();
			db.CreateTable<TaxRate> ();

			if (db.Table<Account> ().Count () == 0) 
			{
				Account a1 = new Account ();
				a1.Name = "Försäljning";
				a1.AccountNumber = 1001;
				a1.IsIncome = true;

				Account a2 = new Account ();
				a2.Name = "Försäljning av tjänster";
				a2.AccountNumber = 1002;
				a2.IsIncome = true;

				Account a4 = new Account ();
				a4.Name = "Övriga egna uttag";
				a4.AccountNumber = 2001;
				a4.IsIncome = false;

				Account a5 = new Account ();
				a5.Name = "Förbruksinventarier";
				a5.AccountNumber = 2002;
				a5.IsIncome = false;

				Account a6 = new Account ();
				a6.Name = "Reklam och PR";
				a6.AccountNumber = 2003;
				a6.IsIncome = false;

				Account a7 = new Account ();
				a7.Name = "Privat";
				a7.AccountNumber = 3001;
				a7.IsIncome = false;

				Account a8 = new Account ();
				a8.Name = "Företaget";
				a8.AccountNumber = 3002;
				a8.IsIncome = false;
			
				db.Insert (a1);
				db.Insert (a2);
				db.Insert (a4);
				db.Insert (a5);
				db.Insert (a6);
				db.Insert (a7);
				db.Insert (a8);
			}

			if (db.Table<TaxRate> ().Count () == 0) 
			{
				TaxRate t1 = new TaxRate ();
				t1.SalesRate = 6;
				TaxRate t2 = new TaxRate ();
				t2.SalesRate = 12;
				TaxRate t3 = new TaxRate ();
				t3.SalesRate = 25;

				db.Insert (t1);
				db.Insert (t2);
				db.Insert (t3);
			}
		}
	}
}
