﻿using System;
using SQLite;

namespace Bookkeeper
{
	/// <summary>
	/// Tax rate Class.
	/// </summary>
	public class TaxRate
	{
		[PrimaryKey, AutoIncrement]
		public int _id{ get; private set;}
		public double SalesRate{ get; set;}

		/// <summary>
		/// Initializes a new instance of the <see cref="Bookkeeper.TaxRate"/> class.
		/// </summary>
		public TaxRate ()
		{
		}
	}
}