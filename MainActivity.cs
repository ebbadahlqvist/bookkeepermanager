﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using SQLite;

namespace Bookkeeper
{
	[Activity (Label = "Bookkeeper", MainLauncher = true, Icon = "@mipmap/icon")]
	/// <summary>
	/// Main activity. Creates an instance of BookKepper and represents the Main activity with different choses for the user.
	/// </summary>
	public class MainActivity : Activity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{

			base.OnCreate (savedInstanceState);
		
			SetContentView (Resource.Layout.Main);
	
			BookkeeperManager bookKeeper = BookkeeperManager.Instance;

			Button newEventButton = FindViewById<Button> (Resource.Id.newEventButton);
			Button showEventsButton = FindViewById<Button> (Resource.Id.showEventButton);
			Button createReportButton = FindViewById<Button> (Resource.Id.createReportButton);
			
			newEventButton.Click += delegate {
				Intent intent = new Intent(this, typeof(CreateNewEvent));
				StartActivity(intent);
			};

			showEventsButton.Click += delegate {
				Intent intent = new Intent(this, typeof(EntryListActivity));
				StartActivity(intent);
			};

			createReportButton.Click += delegate {
				Intent intent = new Intent(this, typeof(CreateReportsActivity));
				StartActivity(intent);
			};
		}
	}
}