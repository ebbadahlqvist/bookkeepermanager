﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper
{
	[Activity (Label = "CreateReportsActivity")]	
	/// <summary>
	/// Create reports activity.
	/// </summary>
	public class CreateReportsActivity : Activity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView (Resource.Layout.CreateReportsActivity);

			Button momsButton = FindViewById<Button> (Resource.Id.moms_button);
			BookkeeperManager bookkeeper = BookkeeperManager.Instance;

			momsButton.Click += delegate {

				Console.WriteLine ("TaxReport= "+bookkeeper.GetTaxReport ());
				var email = new Intent (Android.Content.Intent.ActionSend);
				email.PutExtra (Android.Content.Intent.ExtraSubject, "TaxReport");
				email.PutExtra (Android.Content.Intent.ExtraText,bookkeeper.GetTaxReport());
				email.SetType ("message/rfc822");
				StartActivity(email);
			
			};
		}
	}
}