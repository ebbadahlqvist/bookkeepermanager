﻿using System;
using SQLite;

namespace Bookkeeper
{
	/// <summary>
	/// Class that represents Entry.
	/// </summary>
	public class Entry
	{
		[PrimaryKey, AutoIncrement]
		public int _id{ get; private set;}
		public int AccountId{ get; set;}
		public int TaxRateId{ get; set;}
		public string Date{ get; set;}
		public string Type{ get; set;}
		public string Description{ get; set;}
		public int Money{ get; set; }
		public bool IsIncome{ get; set;}

		/// <summary>
		/// Initializes a new instance of the <see cref="Bookkeeper.Entry"/> class.
		/// </summary>
		public Entry ()
		{
			
		}
	}
}

