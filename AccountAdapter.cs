﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper
{
	[Activity (Label = "AccountAdapter")]			
	public class AccountAdapter : BaseAdapter
	{
		private Activity activity;
		private List<Account> accounts;
		public AccountAdapter(Activity activity, List<Account> accounts)
		{
			this.activity = activity;
			this.accounts = accounts;
		}

		#region implemented abstract members of BaseAdapter
		public override Java.Lang.Object GetItem (int position)
		{
			throw new NotImplementedException ();
		}
		public override long GetItemId (int position)
		{
			return position;
		}
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			if (convertView == 0)
			{
				convertView = activity.LayoutInflater.Inflate ();
			}
		}
		public override int Count {
			get {
				return accounts.Count;
			}
		}
		#endregion
		
	}
}

