﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;

namespace Bookkeeper
{
	[Activity (Label = "EntryListActivity")]
	/// <summary>
	/// Entry list activity.
	/// </summary>
	public class EntryListActivity : Activity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.EntryListActivity);

			BookkeeperManager bookKeeperManager = BookkeeperManager.Instance;
			string path = System.Environment.GetFolderPath
				(System.Environment.SpecialFolder.Personal);
			SQLiteConnection db = new SQLiteConnection (path + @"\database.db");

			List <string> entryDates = new List<string>();
			List <Entry> entries = new List<Entry> ();
			var entryInDb = db.Table<Entry>();

			foreach (Entry e in entryInDb)
			{
				entryDates.Add(e.Date);
				entries.Add (e);
			}

			EntryAdapter entryAdapter = new EntryAdapter (this,entries);
			FindViewById<ListView>(Resource.Id.listView).Adapter = entryAdapter;
		}
	}
}